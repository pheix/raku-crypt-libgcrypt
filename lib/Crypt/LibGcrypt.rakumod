use NativeCall;
use NativeLibs:ver<0.0.7+>:auth<github:salortiz>;
use Crypt::LibGcrypt::Constants;

sub LIBGCRYPT is export
{
    NativeLibs::Searcher.at-runtime(
    'gcrypt',
    'gcry_check_version',
    20).()
}

sub gcry_check_version(Str $req_version --> Str) is native(LIBGCRYPT) {}

sub gcry_free(Pointer)
    is native(LIBGCRYPT) {}

sub gcry_control0(uint32 $cmd --> uint32)
    is native(LIBGCRYPT) is symbol('gcry_control') {}
sub gcry_control1(uint32 $cmd, uint32 --> uint32)
    is native(LIBGCRYPT) is symbol('gcry_control') {}
sub gcry_control2(uint32 $cmd, uint32, uint32 --> uint32)
    is native(LIBGCRYPT) is symbol('gcry_control') {}

class X::Crypt::LibGcrypt is Exception
{
    has uint32 $.code;

    sub gcry_strsource(uint32 $err --> Str) is native(LIBGCRYPT) {}
    sub gcry_strerror(uint32 $err --> Str) is native(LIBGCRYPT) {}

    method message()
    {
        "Failure: {gcry_strsource($!code)}/{gcry_strerror($!code)}"
    }
}

class X::Crypt::LibGcrypt::BadVersion is X::Crypt::LibGcrypt
{
    method message()
    {
        'Incompatible libgcrypt version: ' ~ gcry_check_version(Str)
    }
}

class X::Crypt::LibGcrypt::NoHandle is X::Crypt::LibGcrypt
{
    method message() { 'No handle' }
}

class X::Crypt::LibGcrypt::Invalid is X::Crypt::LibGcrypt
{
    method message() { 'Invalid' }
}

class X::Crypt::LibGcrypt::BadMode is X::Crypt::LibGcrypt
{
    has Str $.mode;
    method message() { "Unknown Cipher Mode $!mode" }
}

class X::Crypt::LibGcrypt::BadFormat is X::Crypt::LibGcrypt
{
    has Str $.format;
    method message() { "Unknown Format $!format" }
}

class X::Crypt::LibGcrypt::BadAlgorithm is X::Crypt::LibGcrypt
{
    has Cool $.algorithm;
    method message() { "Unknown Algorithm $!algorithm" }
}

class X::Crypt::LibGcrypt::ExtendedOutput is X::Crypt::LibGcrypt
{
    has Str $.algorithm;
    method message() { "$!algorithm has extended output, request digest size" }
}

class Crypt::LibGcrypt
{
    method version(--> Str:D) { gcry_check_version(Str) }

    multi method check(0) { 0 }
    multi method check($code) { die X::Crypt::LibGcrypt.new(:$code) }

    sub gcry_get_config(int32, Str --> Pointer)
    is native(LIBGCRYPT) {}

    method config(Str $what? --> Str:D)
    {
        try  # Intermittent return of garbage characters that break utf-8
        {
            my $ptr = gcry_get_config(0, $what) // die;
            my $str = nativecast(Str, $ptr);
            gcry_free($ptr);
            return $str
        }
        return "Unknown configuration" if $!;
    }

    multi method control(UInt:D $cmd)
    {
        $.check: gcry_control0($cmd)
    }

    multi method control(UInt:D $cmd, Int:D $arg1)
    {
        $.check: gcry_control1($cmd, $arg1)
    }

    multi method control(UInt:D $cmd, Int:D $arg1, Int:D $arg2)
    {
        $.check: gcry_control2($cmd, $arg2)
    }
}

INIT
{
    Crypt::LibGcrypt.version;
    Crypt::LibGcrypt.control: GCRYCTL_DISABLE_SECMEM, 0;
    Crypt::LibGcrypt.control: GCRYCTL_INITIALIZATION_FINISHED, 0;
}
